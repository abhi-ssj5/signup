//
//  ViewController.swift
//  sign-up-form
//
//  Created by Sierra 4 on 31/01/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var segmentGender: UISegmentedControl!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var pickerDOB: UIDatePicker!
    @IBOutlet weak var pickerCountryCode: UIPickerView!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtAboutYou: UITextView!
    @IBOutlet weak var switchAgreeToTerms: UISwitch!
    @IBOutlet weak var btnCreateAccount: UIButton!
    var selectedDate: String = ""
    var coCode: String = ""
    
    var countryCode: [String] = ["+91", "+92", "+93", "+94", "+96"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        pickerCountryCode.delegate = self
        pickerCountryCode.dataSource = self
        
        self.addDoneButtonOnKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
 
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.done, target: self, action: #selector(ViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.txtPhoneNo.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        self.txtPhoneNo.resignFirstResponder()
        txtAboutYou.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { // called when 'return' key pressed. return NO to ignore.
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryCode[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryCode.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag >= 0 || pickerView.tag <= countryCode.count {
            coCode =  countryCode[row]
            print(coCode)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.text = countryCode[row]
        pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
    }
 
    @IBAction func datePickerChanged(_ sender: Any) {
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = DateFormatter.Style.short
        selectedDate = dateformatter.string(from: pickerDOB.date)
        print(selectedDate)
    }
    
    @IBAction func createAccount(_ sender: Any) {
        let fName = txtFirstName.text
        let lName = txtLastName.text
        let email = txtEmail.text
        let password = txtPassword.text
        let confirmPassword = txtConfirmPassword.text
        let phoneNo = txtPhoneNo.text
        var gender: String = ""
        let aboutYou = txtAboutYou.text
        let dateOfBirth = selectedDate
        var proceed: Bool = true
        
        if segmentGender.selectedSegmentIndex == 0 {
            gender = "Male"
        }
        else if segmentGender.selectedSegmentIndex == 1 {
            gender = "Female"
        }
        else {
            proceed = false
            displayAlert("Gender not selected")
        }
        
        if fName!.isEmpty || !checkName(value: fName!, field: "First Name") {
            proceed = false
            displayAlert("First Name is either Empty or Invalid !")
        }
        
        if lName!.isEmpty || !checkName(value: lName!, field: "Last Name") {
            proceed = false
            displayAlert("Last Name is either Empty or invalid !")
        }
        
        if email!.isEmpty || !checkEmail(email!) {
            proceed = false
            displayAlert("Email is either Empty or Invalid !")
        }
        
        if password!.isEmpty || confirmPassword!.isEmpty {
            proceed = false
            displayAlert("Either one or both password fields is/are Empty !")
        }
        
        if !checkPsswdLength(password!) {
            displayAlert("Passwords length should be minimum 8 characters !")
            proceed = false
        }
        
        if password != confirmPassword {
            displayAlert("Passwords do not match")
            proceed = false
        }
        
        if dateOfBirth.isEmpty {
            displayAlert("Date of Birth not Selected")
            proceed = false
        }
        
        if coCode.isEmpty {
            displayAlert("Country code not Selected")
            proceed = false
        }
        
        if (phoneNo!.isEmpty) {
            displayAlert("Phone number empty")
            proceed = false
        }
        proceed = checkPhoneNumber(phoneNo!)
        
        if aboutYou!.isEmpty {
            displayAlert("About you empty")
            proceed = false
        }
        
        if !switchAgreeToTerms.isOn {
            displayAlert("Please agree to our terms before proceeding")
            proceed = false
        }
        
        if proceed == true {
            displayAlert("Account successfully created !")
        }
    }
    
    func checkName(value: String, field: String) -> Bool
    {
        let letters = CharacterSet.letters
        if value.characters.count > 25 || value.characters.count < 5 {
            displayAlert("\(field) exceeding character limit")
            return false
        }
        for char in value.unicodeScalars {
            if !letters.contains(char) {
                displayAlert("Invalid \(field)")
                return false
            }
        }
        return true
    }
    
    func checkEmail(_ value: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: value) {
            return false
        }
        return true
    }
    
    func checkPsswdLength(_ pswd: String) -> Bool {
        if pswd.characters.count < 8 {
            return false
        }
        return true
    }
    
    func checkPhoneNumber(_ phone: String) -> Bool {
        let numbers = CharacterSet.decimalDigits
        if String(phone.characters[phone.characters.startIndex]) == "0" {
            displayAlert("Invalid Phone number")
            return false
        }
        if phone.characters.count > 10 || phone.characters.count < 10 {
            displayAlert("Invalid Phone number")
            return false
        }
        for number in phone.unicodeScalars {
            if !numbers.contains(number) {
                displayAlert("Invalid Phone number")
                return false
            }
        }
        return true
    }
    
    func displayAlert(_ userMessage: String) {
        let alertMessage = UIAlertController(title:"Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: nil)
        alertMessage.addAction(okAction)
        self.present(alertMessage, animated: true, completion: nil)
    }
}

